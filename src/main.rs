extern crate csv;
extern crate serde_derive;

use std::error::Error;
use std::process;
use std::env;
use std::fs::File;

struct Config {
    report_csv: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 4 {
            return Err("Usage: report_csv, exclusion_path, image_name");
        }
        let report_csv = args[1].clone();

        Ok(Config { report_csv })
    }
}

type Record = (String, String, String);

fn get_thing_from_csv(config: &Config) -> Vec<String> {
    let csv_records: Vec<Record> = match read_csv(&config.report_csv) {
        Ok(row) => { row },
        Err(err) => {
            eprintln!("Failed to parse csv: {}\n{}", config.report_csv, err);
            process::exit(1);
        },
    };

    for record in csv_records {
        println!("{}", record.1);
    }
    vec![]
}

fn read_csv(csv_path: &str) -> Result<Vec::<Record>, Box<dyn Error>> {
    let file = match File::open(csv_path) {
        Ok(f) => f,
        Err(err) => {
            eprintln!("Unable to open file: {}. \n{}", csv_path, err);
            process::exit(1);
        }
    };

    let mut rdr = csv::Reader::from_reader(file);
    let mut records: Vec<Record> = vec![];

    for result in rdr.deserialize() {
        let record: Record = result?;
        records.push(record);
    }

    Ok(records)
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    get_thing_from_csv(&config);


}
